
Currency Calculator (Iceland)
=============================

This module provides currency calcuation designed for Icelandic users 
who wish to use currency-information from the Central Bank of Iceland. 
It retrieves currency-information from the Central Bank every time cron 
is run, storing information locally. The Central Bank provides currency 
information for various currencies (e.g., EUR, JPY, USD). 

Users can calculate from any currency to any currency for which the 
Central Bank provides currency information for, using ISK as a proxy. 
They can for example calculate what 1000 JPY are in USD or what 1000 USD 
are in NOK. This is useful, as the Icelandic economy is still protected 
by capital controls, and few foreign banks recognize the currency.

The development of this module was sponsored by Kosmos & Kaos, 
Iceland (www.kosmosandkaos.com).
