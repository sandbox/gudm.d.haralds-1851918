<?php
/**
 * @file
 * Template file for block.
 */
?>
<script>
currencycalcIceland = new currencycalcIceland();
currencycalcIceland.setCurrencyInfo(<?php echo $variables["currency_info_json"]; ?>);
</script>

<h3>Currency Exchange</h3>

<table>
<tbody>

<tr><th class="left"><?php print t('Currency'); ?></th><th><?php print t('Change'); ?></th><th><?php print t('Buy'); ?></th><th><?php print t('Sell'); ?></th><th class="last"></th></tr>

<tr class="odd"><td class="left"><p class="flags isk">ISK</p></td><td>-</td><td>1</td><td>1</td><td class="last"><input id="currencycalc_iceland_amount_field_ISK" type="text" onchange="javascript:currencycalcIceland.calculateAllCurrencies('ISK', this.value);" /></td></tr>

<?php
$i = 0;
$currency_info = $variables["currency_info_arr"];

foreach ($variables["currencies_required"] as $currency_current_tmp):

$currency_info_current = $currency_info[$currency_current_tmp];

if ($i % 2 == 0): ?><tr><td class="left"><?php else: ?><tr class="odd"><td class="left"><?php endif; ?>

<p class="flags <?php echo strtolower($currency_info_current["name"]); ?>"><?php echo $currency_info_current["name"]; ?></p></td><td<?php if ($currency_info_current["change"] < 0): ?> class="neg"<?php elseif ($currency_info_current["change"] > 0): ?> class="pos"<?php endif; ?>><?php echo $currency_info_current["change"] * 100; ?></td><td><?php echo $currency_info_current["buy"]; ?></td><td><?php echo $currency_info_current["sell"]; ?></td><td class="last"><input id="currencycalc_iceland_amount_field_<?php echo $currency_info_current["name"]; ?>" type="text" onchange="javascript:currencycalcIceland.calculateAllCurrencies('<?php echo $currency_info_current["name"]; ?>', this.value);" /></td></tr>

<?php
$i++;
endforeach; ?>


<tr><th colspan="5">Currency data provided by The Central Bank of Iceland</th></tr>

</tbody>
</table>
