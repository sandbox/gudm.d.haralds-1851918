/**
 * @file
 * Javascript class, implements calcuation between currencies.
 */

function currencycalcIceland() {
  this.setCurrencyInfo = function(currencyInfo) {
    this.currencyInfo = currencyInfo;
  }

  this.calculate = function(currencyFrom, currencyTo, amount) {
    tmpAmount = amount;

    if (currencyFrom != "ISK") {
      tmpAmount = amount * this.currencyInfo[currencyFrom].buy;
    }

    if (currencyTo != "ISK") {
      if (this.currencyInfo[currencyTo].sell != 0) {
        tmpAmount = tmpAmount / this.currencyInfo[currencyTo].sell;
      }

      else {
        tmpAmount = 0;
      }
    }
    return tmpAmount;
  };

  this.calculateAllCurrencies = function(currencyFrom, amount) {
    if (!(amount = parseFloat(amount, 10))) {
      return;
    }

    if (currencyFrom != 'ISK') {
      value = this.calculate(currencyFrom, "ISK", amount);
      jQuery("#currencycalc_iceland_amount_field_" + "ISK").val(value.toFixed(2));
    }

    for (var keyTmp in this.currencyInfo) {
      if (this.currencyInfo[keyTmp].name == currencyFrom) {
        continue;
      }

      value = this.calculate(currencyFrom, this.currencyInfo[keyTmp].name, amount);
      jQuery("#currencycalc_iceland_amount_field_" + this.currencyInfo[keyTmp].name).val(value.toFixed(2));
    }
  };
}
